﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControlTower
{
    public class Flight
    {
        private string fligth;
        private string route;
        private DateTime time;

        /// <summary>
        /// Constructors
        /// </summary>
        public Flight()
        { }

        public Flight(string flight, string route)
        {
            this.fligth = flight;
            this.route = route;
        }

        // PROPERTIES //
        public string FlightNumber
        {
            get { return fligth; }
            set { fligth = value; }
        }

        public string Status
        {
            get { return route; }
            set { route = value; }
        }

        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
    }
}
