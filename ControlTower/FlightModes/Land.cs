﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTower
{
    public class Land : EventArgs
    {
        private string name;
        private DateTime time;

        public Land()
        {
        }

        public Land(string flightNumber)
        {
            this.name = flightNumber;
            this.time = DateTime.Now;
        }

        public string FlightNumber
        {
            get { return name; }
            set { name = value; }
        }

        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
    }
}
