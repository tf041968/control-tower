﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTower
{
    /// <summary>
    /// Enums for route combo box
    /// </summary>
    public enum FlightRoutes
    {
        Change_Route,
        East,
        West,
        North,
        South
    }

    public class ChangeRoute : EventArgs
    {
        private DateTime time;
        private string route;
        private string flight;

        /// <summary>
        /// Constructors
        /// </summary>
        public ChangeRoute()
        {
        }

        public ChangeRoute(string fligthNumber, string route)
            : this()
        {
            this.route = route;
            this.flight = fligthNumber;
            this.time = DateTime.Now;
        }

        // PROPERTIES //
        public string Route
        {
            get { return route; }
            set { route = value; }
        }

        public string Flight
        {
            get { return flight; }
            set { flight = value; }
        }

        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
    }
}
