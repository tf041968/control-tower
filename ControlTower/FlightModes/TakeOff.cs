﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTower
{
    public class TakeOff : EventArgs
    {
        private string flightNumber;
        private string message;
        private DateTime time;

        public TakeOff()
        {
            message = "Started";
            time = DateTime.Now;
        }

        public TakeOff(string flightNumber)
            : this()
        {
            this.flightNumber = flightNumber;
        }

        public string FlightNumber
        {
            get { return flightNumber; }
            set { flightNumber = value; }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
    }
}
