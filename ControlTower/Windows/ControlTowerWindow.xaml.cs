﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ControlTower.Managers;
using ControlTower.Windows;

namespace ControlTower
{
    /// <summary>
    /// Interaction logic for ControlTowerWindow.xaml
    /// </summary>
    public partial class ControlTowerWindow : Window
    {
        public ControlTowerWindow()
        {
            InitializeComponent();
            Title = "ControlTower";
            tbFlightNumber.Text = "ab1234";
        }

        /// <summary>
        /// When user click button to send airplane to runway
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string[] error;
            if (Utilities.CheckFlightNumber(tbFlightNumber.Text, out error))
            {
                Flight flight = new Flight(tbFlightNumber.Text, "Heading for runway");
                flight.Time = DateTime.Now;
                FlightWindow newFlight = new FlightWindow(flight);
                newFlight.Show();
                newFlight.TakeOffMessage += FlightTakeOff;
                newFlight.ChangeRoute += FlightChangeRoute;
                newFlight.Landing += FlightLanding;
                lvFlights.Items.Insert(0, new Flight
                {
                    FlightNumber = flight.FlightNumber,
                    Status = flight.Status,
                    Time = flight.Time
                });
            }
            else
            {
                MessageBox.Show(error[0], error[1]);
            }
        }

        /// <summary>
        /// Subscriber method that handle landings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FlightLanding(object sender, Land e)
        {
            lvFlights.Items.Insert(0, new Flight
            {
                FlightNumber = e.FlightNumber,
                Status = "Has Landed",
                Time = e.Time
            });
        }

        /// <summary>
        /// Subscriber method that handle Changed routes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FlightChangeRoute(object sender, ChangeRoute e)
        {
            lvFlights.Items.Insert(0, new Flight
            {
                FlightNumber = e.Flight,
                Status = e.Route,
                Time = e.Time
            });
        }

        /// <summary>
        /// Subscriber method that handles Take Off
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FlightTakeOff(object sender, TakeOff e)
        {
            lvFlights.Items.Insert(0, new Flight
            {
                FlightNumber = e.FlightNumber,
                Status = e.Message,
                Time = e.Time
            });
        }
    }
}