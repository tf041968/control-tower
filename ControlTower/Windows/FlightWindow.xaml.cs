﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ControlTower.Windows
{
    /// <summary>
    /// Interaction logic for FlightWindow.xaml
    /// </summary>
    public partial class FlightWindow : Window
    {
        private string flightNumber;
        private bool hasLanded = false;
        private Flight flight;
        public event EventHandler<TakeOff> TakeOffMessage;
        public event EventHandler<ChangeRoute> ChangeRoute;
        public event EventHandler<Land> Landing;

        /// <summary>
        /// Constructors
        /// </summary>
        public FlightWindow()
        {
            InitializeComponent();
            InitializeGUI();
            flight = new Flight();
        }

        public FlightWindow(string flightNumber)
            : this()
        {
            this.flightNumber = flightNumber;
            this.Title = "Flight: " + FlightNumber;
            SetCarrierImage();
        }

        public FlightWindow(Flight flight)
            : this()
        {
            flightNumber = flight.FlightNumber;
            Title = "Flight: " + FlightNumber;
            SetCarrierImage();
        }

        /// <summary>
        /// Initialize the gui
        /// </summary>
        private void InitializeGUI()
        {
            btnLand.IsEnabled = false;
            comboRoute.IsEnabled = false;
            btnStart.IsEnabled = true;
            comboRoute.ItemsSource = Enum.GetValues(typeof(FlightRoutes));
            comboRoute.SelectedIndex = 0;
        }

        /// <summary>
        /// When user click start - Airplane take off. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            btnLand.IsEnabled = true;
            btnStart.IsEnabled = false;
            comboRoute.IsEnabled = true;
            TakeOff takeOff = new TakeOff(flightNumber);
            OnSendTakeOff(takeOff);
        }

        /// <summary>
        /// When user click land - Airplane landing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLand_Click(object sender, RoutedEventArgs e)
        {
            Land landing = new Land(FlightNumber);
            OnLanding(landing);
            hasLanded = true;
            Close();
        }

        /// <summary>
        /// Publisher for take offs
        /// </summary>
        /// <param name="e"></param>
        private void OnSendTakeOff(TakeOff e)
        {
            if (TakeOffMessage != null)
            {
                TakeOffMessage(this, e);
            }
        }

        /// <summary>
        /// Publisher for Landings
        /// </summary>
        /// <param name="e"></param>
        private void OnLanding(Land e)
        {
            if (Landing != null)
            {
                Landing(this, e);
            }
        }

        /// <summary>
        /// Publisher for changing routes.
        /// </summary>
        /// <param name="e"></param>
        private void OnChangedRoute(ChangeRoute e)
        {
            if (ChangeRoute != null)
            {
                ChangeRoute(this, e);
            }
        }

        /// <summary>
        /// Sets carrier image based on the 2 letters in the flight number
        /// </summary>
        private void SetCarrierImage()
        {
            string[] fligthPrefix = Regex.Split(FlightNumber, "[1-9]");
            Uri uri;
            switch (fligthPrefix[0])
            {
                case "aa":
                    uri = new Uri(@"pack://application:,,,/gfx/aa.jpeg", UriKind.Absolute);
                    break;
                case "ba":
                    uri = new Uri(@"pack://application:,,,/gfx/ba.jpeg", UriKind.Absolute);
                    break;
                case "ja":
                    uri = new Uri(@"pack://application:,,,/gfx/jal.jpeg", UriKind.Absolute);
                    break;
                default:
                    uri = new Uri(@"pack://application:,,,/gfx/unidentifiedCarrier.jpg", UriKind.Absolute);
                    break;
            }
            ImageSource imgSource = new BitmapImage(uri);
            imgAirline.Source = imgSource;
        }

        /// <summary>
        /// When route is changed. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboRoute_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FlightRoutes selectedRoute = (FlightRoutes)comboRoute.SelectedIndex;
            ChangeRoute changeRoute;
            string route = "Heading ";
            if (FlightNumber != null || selectedRoute != FlightRoutes.Change_Route)
            {
                switch (selectedRoute)
                {
                    case FlightRoutes.West:
                        route += FlightRoutes.West.ToString();
                        break;
                    case FlightRoutes.East:
                        route += FlightRoutes.East.ToString();
                        break;
                    case FlightRoutes.North:
                        route += FlightRoutes.North.ToString();
                        break;
                    case FlightRoutes.South:
                        route += FlightRoutes.South.ToString();
                        break;
                }
                changeRoute = new ChangeRoute(FlightNumber, route);
                OnChangedRoute(changeRoute);
            }
        }

        /// <summary>
        /// When window is closing. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!hasLanded)
            {
                ChangeRoute route = new ChangeRoute(FlightNumber, "Connection terminated");
                OnChangedRoute(route);
            }
        }

        /// PROPERTIES ///
        public string FlightNumber
        {
            get { return flightNumber; }
            set { flightNumber = value; }
        }
    }
}
