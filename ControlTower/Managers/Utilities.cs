﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ControlTower.Managers
{
    class Utilities
    {
        public static bool CheckFlightNumber(string flightNumber, out string[] message)
        {
            message = null;
            if (!Regex.IsMatch(flightNumber, "[A-Za-z]{2}[0-9]{4}$"))
            {
                message = new string[2]{"The flight number must consist of 2 letters \nfollowed by 4 digits", "Incorrect flight number" }
                ;
                return false;
            }
            return true;
        }
    }
}
